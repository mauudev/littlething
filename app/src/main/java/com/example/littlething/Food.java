package com.example.littlething;

import java.util.Random;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;

public class Food {
	private float x;
	private float y;
	private GameView gameView;
	private Bitmap bmp;
	private float width;
	private float height;
	
	public Food(GameView gameView, Bitmap bmp){
			this.gameView = gameView;
			this.bmp = bmp;
			this.width = bmp.getWidth();
			this.height = bmp.getHeight();
			Random rnd = new Random();
			x = rnd.nextInt(gameView.getWidth() - (int)width);
			y = rnd.nextInt(gameView.getHeight() - (int)height);
	}
	public void onDraw(Canvas canvas){
		canvas.drawBitmap(bmp,x,y,null);
	}
	public float getX(){return x;}
	public float getY(){return y;}
	public boolean isSpriteCollision(float x2, float y2) {
		return x2 > x && x2 < x + width && y2 > y && y2 < y + height;
	}
}
