package com.example.littlething;


import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;

public class GoodSprite {
	private static final int BMP_COLUMNS = 3;
	private static final int BMP_ROWS = 3;
	private int x;
	private int y;
	private int xSpeed;
	private int ySpeed;
	private GameView gameView;
	private Bitmap bmp;
	private int width;
	private int height;
	private int currentFrame;

	public GoodSprite(GameView gameView, Bitmap bmp){
		this.gameView = gameView;
		this.bmp = bmp;
		this.width = bmp.getWidth() / BMP_COLUMNS;
		this.height = bmp.getHeight() / BMP_ROWS;
		x = gameView.getWidth() - width;
		y = gameView.getHeight() - height;
		xSpeed = 0;
		ySpeed = 0;
	}
	private void update(){
		if(x > gameView.getWidth() - width - xSpeed || x + xSpeed < 0)
			xSpeed = - xSpeed;
	   	 x = x + xSpeed;
		if(y > gameView.getHeight() - height - ySpeed || y + ySpeed < 0)
			ySpeed = -ySpeed;
		 y = y + ySpeed;	
		 currentFrame = ++currentFrame % BMP_COLUMNS;
	}
	public void onDraw(Canvas canvas){
		update();
		int srcX = currentFrame * width;
		int srcY = 1 * height;
		Rect src = new Rect(srcX, srcY, srcX + width, srcY + height);
		Rect dst = new Rect(x, y, x + width, y  + height);
		canvas.drawBitmap(bmp,src,dst,null);
	}
	private void setXSpeed(int speed){xSpeed = speed;}
	private void setYSpeed(int speed){ySpeed = speed;}
	private void setX(int x){this.x = x;}
	private void setY(int y){this.y = y;}
	public int getX(){return x;}
	public int getY(){return y;}
	public void setSpeeds(int xs, int ys){
		xSpeed = xs;
		ySpeed = ys;
	}
	public void setPosition(int x, int y){
		this.x = x;
		this.y = y;
	}
	public void goToThePoint(int x1, int y1){
		int aux = points((int)x,(int)y,x1,y1);
		if(x1 > x && x1 < x + width){
			setSpeeds(7,0);
		}else if(y1 > y && y1 < y + height) setSpeeds(0,7);
		switch(aux){
		case 0:
			break;
		case 1:
			setSpeeds(-7,-7);
			if(x == x1) setSpeeds(0,-7);
			if(y == y1) setSpeeds(-7,0);
			if(x == x1 && y == y1) setSpeeds(0,0);
			break;
		case 2:
			setSpeeds(-7,7);
			if(x == x1) setSpeeds(0,7);
			if(y == y1) setSpeeds(-7,0);
			if(x == x1 && y == y1) setSpeeds(0,0);
			break;
		case 3:
			setSpeeds(7,-7);
			if(x == x1) setSpeeds(0,-7);
			if(y == y1) setSpeeds(7,0);
			if(x == x1 && y == y1) setSpeeds(0,0);
			break;
		case 4:
			setSpeeds(7,7);
			if(x == x1) setSpeeds(0,7);
			if(y == y1) setSpeeds(7,0);
			if(x == x1 && y == y1) setSpeeds(0,0);
			break;
		case 5:
			setSpeeds(0,-7);
			if(x == x1 && y == y1) setSpeeds(0,0);
			break;
		case 6:
			setSpeeds(0,7);
			if(x == x1 && y == y1) setSpeeds(0,0);
			break;
		case 7:
			setSpeeds(-7,0);
			if(x == x1 && y == y1) setSpeeds(0,0);
			break;
		case 8:
			setSpeeds(7,0);
			if(x == x1 && y == y1) setSpeeds(0,0);
			break;
		}
	
	}

	private int points(int a, int b, int c, int d) {
		int res = 0;
		if (a > c && b > d) res = 1;
		if (a > c && b < d) res = 2;
		if (a < c && b > d) res = 3;
		if (a < c && b < d) res = 4;
		if (a == c && b > d)res = 5;
		if (a == c && b < d)res = 6;
		if (a > c && b == d)res = 7;
		if (a < c && b == d)res = 8;
		return res;
	}
	public boolean isCollision(int x2, int y2) {
		return x2 > x && x2 < x + width && y2 > y && y2 < y + height;
	}

}
