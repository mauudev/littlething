package com.example.littlething;


import android.app.Activity;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.TextView;

public class MainActivity extends Activity implements OnCompletionListener{
	MediaPlayer reproductor;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(new GameView(this));

	}
	public void onCompletion(MediaPlayer reproductor) {
		reproductor.start();
	}
		
	protected void onStart() {
		super.onStart();
		reproductor= MediaPlayer.create(this, R.raw.bmg);
		reproductor.setOnCompletionListener(this);
		reproductor.start();
	}

	protected void onStop() {
		super.onStop();
		reproductor.stop();
		reproductor.release();
	}
}
