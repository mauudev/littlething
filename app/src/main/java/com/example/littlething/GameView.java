package com.example.littlething;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;

@SuppressLint("WrongCall")
public class GameView extends SurfaceView {

	private Bitmap bmpMac;
	private SurfaceHolder holder;
	private GameLoopThread gameLoop;
	private List<BadSprite> badSprites = new ArrayList<BadSprite>();
	private List<GoodSprite> goodSprites = new ArrayList<GoodSprite>();
	private long lastClick;
	private Bitmap bmpMac2;
	private Bitmap bmpDead;
	private List<TempSprite> temps = new ArrayList<TempSprite>();
	private List<Food> foods = new ArrayList<Food>();
	private MediaPlayer mp1;
	private MediaPlayer mp2;
	private MediaPlayer mp3;
	private float posX;
	private float posY;

	public GameView(Context context) {
		super(context);
		gameLoop = new GameLoopThread(this);
		holder = getHolder();
		mp1 = MediaPlayer.create(context, R.raw.eat);
		mp2 = MediaPlayer.create(context, R.raw.applause);
		mp3 = MediaPlayer.create(context, R.raw.fold);
		holder.addCallback(new Callback() {

			@Override
			public void surfaceDestroyed(SurfaceHolder arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void surfaceCreated(SurfaceHolder arg0) {
				createSprites();
				gameLoop.setRunning(true);
				gameLoop.start();
			}

			@Override
			public void surfaceChanged(SurfaceHolder holder, int format,
					int width, int height) {
				// TODO Auto-generated method stub

			}
		});
		bmpMac2 = BitmapFactory.decodeResource(getResources(), R.drawable.mac2);
		bmpMac = BitmapFactory.decodeResource(getResources(), R.drawable.mac);
		bmpDead = BitmapFactory.decodeResource(getResources(), R.drawable.deadandroid);

	}

	private void createSprites() {
		badSprites.add(createBadSprite(R.drawable.badandroid));
		badSprites.add(createBadSprite(R.drawable.badandroid));
		badSprites.add(createBadSprite(R.drawable.badandroid));
		badSprites.add(createBadSprite(R.drawable.badandroid));
		badSprites.add(createBadSprite(R.drawable.badandroid));
		badSprites.add(createBadSprite(R.drawable.badandroid));
		badSprites.add(createBadSprite(R.drawable.badandroid));
		badSprites.add(createBadSprite(R.drawable.badandroid));
		badSprites.add(createBadSprite(R.drawable.badandroid));
		badSprites.add(createBadSprite(R.drawable.badandroid));
		badSprites.add(createBadSprite(R.drawable.badandroid));
		badSprites.add(createBadSprite(R.drawable.badandroid));
		badSprites.add(createBadSprite(R.drawable.badandroid));
		badSprites.add(createBadSprite(R.drawable.badandroid));
		badSprites.add(createBadSprite(R.drawable.badandroid));
		badSprites.add(createBadSprite(R.drawable.badandroid));
		badSprites.add(createBadSprite(R.drawable.badandroid));
		badSprites.add(createBadSprite(R.drawable.badandroid));
		badSprites.add(createBadSprite(R.drawable.badandroid));
		badSprites.add(createBadSprite(R.drawable.badandroid));
		badSprites.add(createBadSprite(R.drawable.badandroid));
		badSprites.add(createBadSprite(R.drawable.badandroid));
		badSprites.add(createBadSprite(R.drawable.badandroid));
		badSprites.add(createBadSprite(R.drawable.badandroid));
		badSprites.add(createBadSprite(R.drawable.badandroid));
		badSprites.add(createBadSprite(R.drawable.badandroid));
		badSprites.add(createBadSprite(R.drawable.badandroid));
		
		goodSprites.add(createGoodSprite(R.drawable.android));
		foods.add(createFood(R.drawable.mac));
		foods.add(createFood(R.drawable.mac));
		foods.add(createFood(R.drawable.mac));
		foods.add(createFood(R.drawable.mac));
		foods.add(createFood(R.drawable.mac));
		foods.add(createFood(R.drawable.mac));
		foods.add(createFood(R.drawable.mac));
		foods.add(createFood(R.drawable.mac));
		foods.add(createFood(R.drawable.mac));
		foods.add(createFood(R.drawable.mac));
		foods.add(createFood(R.drawable.mac));
		foods.add(createFood(R.drawable.mac));		
		foods.add(createFood(R.drawable.mac));
		foods.add(createFood(R.drawable.mac));
		foods.add(createFood(R.drawable.mac));
		foods.add(createFood(R.drawable.mac));
		foods.add(createFood(R.drawable.mac));
		foods.add(createFood(R.drawable.mac));

	}

	private BadSprite createBadSprite(int resource) {
		Bitmap bmp = BitmapFactory.decodeResource(getResources(), resource);
		return new BadSprite(this, bmp);
	}

	private Food createFood(int resource) {
		Bitmap bmp = BitmapFactory.decodeResource(getResources(), resource);
		return new Food(this, bmp);
	}

	private GoodSprite createGoodSprite(int resource) {
		Bitmap bmp = BitmapFactory.decodeResource(getResources(), resource);
		return new GoodSprite(this, bmp);
	}

	@Override
	public void onDraw(Canvas canvas) {
		canvas.drawColor(Color.BLACK);
		for (int i = temps.size() - 1; i >= 0; i--)
			temps.get(i).onDraw(canvas);
		for (BadSprite sp : badSprites)
			sp.onDraw(canvas);
		for (Food food : foods)
			food.onDraw(canvas);
		for (GoodSprite sp : goodSprites)
			sp.onDraw(canvas);
		GoodSprite gSprite = goodSprites.get(0);
		for (int i = foods.size() - 1; i >= 0; i--) {
			Food food = foods.get(i);
			if (food.isSpriteCollision(gSprite.getX(), gSprite.getY())) {
				mp1.start();
				foods.remove(food);
				if (foods.isEmpty()) {
					mp2.start();
					for (BadSprite bsprite : badSprites)
						bsprite.setSpeeds(0, 0);
					goodSprites.get(0).setSpeeds(0, 0);
				}
				temps.add(new TempSprite(temps, this, food.getX(), food.getY(),
						bmpMac2));
				break;
			}
		}

		for (int j = badSprites.size() - 1; j >= 0; j--) {
			BadSprite bs = badSprites.get(j);
			wakeUp(bs,foods,bs.getXSpeed(),bs.getYSpeed());
			if (bs.isCollision(gSprite.getX(), gSprite.getY())) {
				mp3.start();
				goodSprites.remove(0);
				if (goodSprites.isEmpty()) {
					for (BadSprite bsprite : badSprites)
						bsprite.setSpeeds(0, 0);
					temps.add(new TempSprite(temps, this, gSprite.getX(),
							gSprite.getY(), bmpDead));
					break;
				}
			}
		}

	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (!goodSprites.isEmpty()) {
			if (System.currentTimeMillis() - lastClick > 300) {
				lastClick = System.currentTimeMillis();
				synchronized (getHolder()) {
					float x = event.getX();
					float y = event.getY();
					GoodSprite gSprite = goodSprites.get(0);
					gSprite.goToThePoint((int) x, (int) y);
				}
			}
		}
		return true;
	}
	private void wakeUp(BadSprite bds,List<Food> list,int xs, int ys){
		Random rnd = new Random();
		if(xs == 0 && ys == 0 && !list.isEmpty())
			bds.setSpeeds(rnd.nextInt(10)-5, rnd.nextInt(10)-5);
	}
}