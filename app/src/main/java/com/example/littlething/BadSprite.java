package com.example.littlething;


import java.util.Random;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;

public class BadSprite {
	private static final int BMP_COLUMNS = 3;
	private static final int BMP_ROWS = 3;
	private int x;
	private int y;
	private int xSpeed;
	private int ySpeed;
	private GameView gameView;
	private Bitmap bmp;
	private int width;
	private int height;
	private int currentFrame;
	private Random rnd;

	
	public BadSprite(GameView gameView, Bitmap bmp){
		this.gameView = gameView;
		this.bmp = bmp;
		this.width = bmp.getWidth() / BMP_COLUMNS;
		this.height = bmp.getHeight() / BMP_ROWS;
		rnd = new Random();
		x = 0;
		y = 0;
		xSpeed = rnd.nextInt(10)-5;
		ySpeed = rnd.nextInt(10)-5;
	}
	private void update(){
		 if(x > gameView.getWidth() - width - xSpeed){
			 int tmp = rnd.nextInt(8);
			 if(tmp == 0) tmp = rnd.nextInt(8);
			 if(ySpeed == 0) ySpeed = tmp;	 
			 xSpeed = -tmp;
		 }
		 if(x + xSpeed < 0){
			 int tmp = rnd.nextInt(8);
			 if(tmp == 0) tmp = rnd.nextInt(8);
			 if(ySpeed == 0) ySpeed = tmp;
			 xSpeed = tmp;
		 }
		 x = x + xSpeed;
		 if(y > gameView.getHeight() - height - ySpeed){
			 int tmp = rnd.nextInt(8);
			 if(tmp == 0) tmp = rnd.nextInt(8);
			 if(xSpeed == 0) xSpeed = tmp;
			 ySpeed = -tmp;
		 }
		 if(y + ySpeed < 0){
			 int tmp = rnd.nextInt(8);
			 if(tmp == 0) tmp = rnd.nextInt(8);
			 if(xSpeed == 0) xSpeed = tmp;
			 ySpeed = tmp;
		 }
		 y = y + ySpeed;
		 currentFrame = ++currentFrame % BMP_COLUMNS;
	}
	public void onDraw(Canvas canvas){
		update();
		int srcX =  (currentFrame * width);
		int srcY = (1 * height);
		Rect src = new Rect(srcX, srcY, srcX + width, srcY + height);
		Rect dst = new Rect(x, y, x + width, y  + height);
		canvas.drawBitmap(bmp,src,dst,null);
	}
	public int getXSpeed(){return xSpeed;}
	public int getYSpeed(){return ySpeed;}
	public void setSpeeds(int x, int y){
		this.xSpeed = x;
		this.ySpeed = y;
	}
	public boolean isCollision(float x2, float y2) {
		return x2 > x && x2 < x + width && y2 > y && y2 < y + height;
	}

}
